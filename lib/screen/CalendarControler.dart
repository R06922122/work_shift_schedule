import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarControler extends StatelessWidget {
  calendarController: _calendarController;
  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TableCalendar(
      calendarController: _calendarController,
    );
  }

  /* try section */
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //       appBar: AppBar(
  //         title: Text("Hello"),
  //       ),
  //       body: Column(
  //         children: [
  //           Container(
  //             decoration: BoxDecoration(
  //               color: Colors.red,
  //             ),
  //             child: Text("hi there"),
  //           ),
  //         ],
  //       ));
  // }å
}
